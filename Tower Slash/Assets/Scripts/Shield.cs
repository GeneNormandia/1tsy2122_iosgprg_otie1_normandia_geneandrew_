using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    public GameManager game;
    // Start is called before the first frame update
    void Start()
    {
        game = FindObjectOfType<GameManager>();
    }



    // Update is called once per frame
    void Update()
    {
        transform.position -= new Vector3(0, .1f, 0);   
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "PlayerCollider")
        {
            Debug.Log("Collided");
            game.health += 1;
            //AddHealth();
            Destroy(gameObject);
        }
    }
}
