using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    float speedU = .02f; //unity
    float speedM = .2f; //mobile

    float reverseSpeedU = .002f;//unity
    float reverseSpeedM = .02f;//mobile

    private Vector2 startTouchPosition;
    private Vector2 currentPosition;
    private Vector2 endTouchPosition;
    private bool stopTouch = false;

    public float swipeRange;
    public float tapRange;

    public int inTrigger = 0;

    public GameManager game;
    [SerializeField] GameObject dkSprite;//dksprite

    // Start is called before the first frame update
    void Start()
    {
        game = FindObjectOfType<GameManager>();
    }

   

    // Update is called once per frame
    void Update()
    {
        Tap();
        if(transform.position.y > -3.2f)
        {
            transform.position -= new Vector3(0, .05f, 0);
        }
    }

    void Tap()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            startTouchPosition = Input.GetTouch(0).position;
        }

        //tap
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            stopTouch = false;

            endTouchPosition = Input.GetTouch(0).position;

            Vector2 Distance = endTouchPosition - startTouchPosition;
            if (inTrigger == 0)
            {
                if (Mathf.Abs(Distance.x) < tapRange && Mathf.Abs(Distance.y) < tapRange)
                {
                    game.points += 1;
                    transform.position += new Vector3(0, 1.5f, 0);
                }
            }
            else
            {

            }
        }
    }

    
}
