using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Text textHp;
    public Text textPoints;
    public Text textDk;

    public int health = 1;
    public int points = 0;
    public int dkActivate = 0;

    public bool doubleKill = false;
    public int inTrigger;

    public Player player;
    [SerializeField] GameObject dkSprite;//dksprite

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        inTrigger = player.inTrigger;
        textHp.text = health.ToString();
        textPoints.text = points.ToString();

        Swipe();

        if (inTrigger == 1)
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                Debug.Log("Left is pressed");
                //dkSprite.transform.position = new Vector3(1.28f, -0.31f, 0);
                dkSprite.transform.position = new Vector3(1.149f, -0.9f, 0);
            }
        }

        if (health <= 0)
        {
            //gameover screen
            SceneManager.LoadScene("GameOver");
        }

        //turn on doublekill
        if (points >= 60)
        {
            textDk.text = "On";
        }

        else
        {

        }
    }

    protected void AddHealth()
    {
        Debug.Log("HP: " + health);
        this.health += 1;
    }




    //swipe
    public Text outputText;
    private Vector2 startTouchPosition;
    private Vector2 currentPosition;
    private Vector2 endTouchPosition;
    private bool stopTouch = false;

    public float swipeRange;
    public float tapRange;

    

    public void Swipe()
    {
        if (points >= 60)//double kill is activated at 30 points
        {
            if (inTrigger == 1)
            {
                if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    startTouchPosition = Input.GetTouch(0).position;
                }

                if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
                {
                    currentPosition = Input.GetTouch(0).position;
                    Vector2 Distance = currentPosition - startTouchPosition;

                    if (!stopTouch)
                    {
                        //left
                        if (Distance.x < -swipeRange)
                        {
                            dkSprite.transform.position = new Vector3(1.149f, -0.9f, 0);
                        }

                        //right
                        else if (Distance.x > swipeRange)
                        {
                            dkSprite.transform.position = new Vector3(1.149f, -0.9f, 0);
                        }

                        //up
                        else if (Distance.y > swipeRange)
                        {
                            dkSprite.transform.position = new Vector3(1.149f, -0.9f, 0);
                        }

                        //down
                        else if (Distance.y < -swipeRange)
                        {
                            dkSprite.transform.position = new Vector3(1.149f, -0.9f, 0);
                        }
                    }
                }
            }
        }
    }


}

