using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Coroutine : MonoBehaviour
{
    [SerializeField] Sprite[] arrowSprites;

    [SerializeField] GameObject arrowDirection;

    [SerializeField] Sprite leftArrow;
    [SerializeField] Sprite rightArrow;
    [SerializeField] Sprite upArrow;
    [SerializeField] Sprite downArrow;

    //Opposites
    [SerializeField] string Opposites;
    [SerializeField] Sprite redUp;
    [SerializeField] Sprite redDown;
    [SerializeField] Sprite redRight;
    [SerializeField] Sprite redLeft;

    public bool inRange = false;

    int setArrow;

    //touch inputs
    private Vector2 startTouchPosition;
    private Vector2 currentPosition;
    private Vector2 endTouchPosition;
    private bool stopTouch = false;

    public float swipeRange;
    public float tapRange;
    private static int point;
    public Text outputText;

    //========


    public GameManager game;
    public Player player;

    
    public DieOnTrigger dieOnDK;//dk script

    [SerializeField] GameObject dkSprite;//dksprite


    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            this.inRange = true;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "PlayerCollider")//Reduce health when collided with player
        {
            game.health -= 1;
            Destroy(gameObject);
            player.inTrigger = 0;
            game.inTrigger = 0;
        }

        //die on dk
        if (other.tag == "test")
        {
            game.points += 1;
            Destroy(gameObject);
            player.inTrigger = 0;
            game.inTrigger = 0;
        }
        //die on dash from combo
        if (other.tag == "dash")
        {
            game.points += 1;
            Destroy(gameObject);
            player.inTrigger = 0;
            game.inTrigger = 0;
        }
    }

    void Start()
    {
        game = FindObjectOfType<GameManager>();
        player = FindObjectOfType<Player>();
        StartCoroutine(Timer());

        dieOnDK = GetComponent<DieOnTrigger>();
        dieOnDK.enabled = false;

        setArrow = Random.Range(0, 7);
    }

    public void Swipe()
    {
        if(inRange)
        {
            player.inTrigger = 1;
            game.inTrigger = 1;
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
              startTouchPosition = Input.GetTouch(0).position;
            }

            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                currentPosition = Input.GetTouch(0).position;
                Vector2 Distance = currentPosition - startTouchPosition;

                if (!stopTouch)
                {
                    //left
                    if (Distance.x < -swipeRange)
                    {

                        if (setArrow == 0 || setArrow == 6)
                        {
                            Destroy(gameObject);
                            stopTouch = true;
                            game.points += 1;
                            player.inTrigger = 0;
                            game.inTrigger = 0;
                        }
                        else
                        {
                            stopTouch = true;
                            game.health-=1;
                        }
                    }

                    //right
                    else if (Distance.x > swipeRange)
                    {

                        if (setArrow == 1 || setArrow == 7)
                        {
                            Destroy(gameObject);
                            stopTouch = true;
                            game.points+=1;
                            player.inTrigger = 0;
                            game.inTrigger = 0;

                        }
                        else
                        {
                            stopTouch = true;
                            game.health-=1;
                        }
                    }

                    //up
                    else if (Distance.y > swipeRange)
                    {

                        if (setArrow == 2 || setArrow == 5)
                        {
                            Destroy(gameObject);
                            stopTouch = true;
                            game.points+=1;
                            player.inTrigger = 0;
                            game.inTrigger = 0;

                        }
                        else
                        {
                            stopTouch = true;
                            game.health-=1;
                        }
                    }

                    //down
                    else if (Distance.y < -swipeRange)
                    {

                        if (setArrow == 3 || setArrow == 4)
                        {
                            Destroy(gameObject);
                            stopTouch = true;
                            game.points+=1;
                            player.inTrigger = 0;
                            game.inTrigger = 0;
                        }
                        else
                        {
                            stopTouch = true;
                            game.health-=1;
                        }
                    }
                }
            }
        }
        
        //tap
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            stopTouch = false;

            endTouchPosition = Input.GetTouch(0).position;

            Vector2 Distance = endTouchPosition - startTouchPosition;

            if (Mathf.Abs(Distance.x) < tapRange && Mathf.Abs(Distance.y) < tapRange)
            {

            }

        }

    }

    void Update()
    {
        Swipe();
    }

    IEnumerator Timer()
    { 
        while(!inRange)
        {
            arrowDirection.GetComponent<SpriteRenderer>().sprite = arrowSprites[Random.Range(0, arrowSprites.Length)];
            yield return new WaitForSeconds(.08f);
        }

        if (setArrow == 0)//left
        {
            arrowDirection.GetComponent<SpriteRenderer>().sprite = leftArrow;
        }
        if (setArrow == 1)//right
        {
            arrowDirection.GetComponent<SpriteRenderer>().sprite = rightArrow;
        }
        if (setArrow == 2)//up
        {
            arrowDirection.GetComponent<SpriteRenderer>().sprite = upArrow;
        }
        if (setArrow == 3)//down
        {
            arrowDirection.GetComponent<SpriteRenderer>().sprite = downArrow;
        }
        //opposites
        if (setArrow == 4)//opposite of up
        {
            arrowDirection.GetComponent<SpriteRenderer>().sprite = redUp;
        }
        if (setArrow == 5)//opposite of down
        {
            arrowDirection.GetComponent<SpriteRenderer>().sprite = redDown;
        }
        if (setArrow == 6)//opposite of right
        {
            arrowDirection.GetComponent<SpriteRenderer>().sprite = redRight;
        }
        if (setArrow == 7)//opposite of left
        {
            arrowDirection.GetComponent<SpriteRenderer>().sprite = redLeft;
        }
    }

}


